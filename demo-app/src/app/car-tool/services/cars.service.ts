import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Car } from '../models/car';

@Injectable({
  providedIn: 'root',
})
export class CarsService {

  private cars: Car[] = [
    { id: 1, make: 'test', model: 't', year: 2003, color: 'red', price: 1 }
  ];

  constructor(private httpClient: HttpClient) { }

  all() {
    return this.httpClient.get<Car[]>('http://localhost:4250/cars');
  }

  one(carId: number) {
    return this.cars.find(c => c.id === carId);
  }

  append(car: Car) {
    // this.cars = this.cars.concat({
    //   id: Math.max(...this.cars.map(c => c.id)) + 1,
    //   ...car,
    // });

    return this.httpClient.post('http://localhost:4250/cars', car).toPromise();
  }

  replace(car: Car) {
    const newCars = this.cars.concat();
    newCars[newCars.findIndex(c => c.id === car.id)] = car;
    this.cars = newCars;
  }

  delete(carId: number) {
    this.cars = this.cars.filter(c => c.id !== carId);
  }


}
