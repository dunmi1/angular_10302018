import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) { }

  id = 0;

  ngOnInit() {
    this.id = Number(this.activatedRoute.snapshot.params.id);

  }

  goHome() {
    this.router.navigateByUrl('/home');
  }

}
