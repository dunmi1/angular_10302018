import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));

// const p = new Promise((resolve, reject) => {

//   console.log('called promise callback');

//   setTimeout(() => {
//     resolve('done');
//   }, 2000);
// });

// p.then(result => console.log(result));
// p.then(result => console.log(result));

// import { Observable } from 'rxjs';
// import { map, filter } from 'rxjs/operators';

// const o = Observable.create(observer => {

//   console.log('called observable callback');

//   let counter = 0;

//   const h = setInterval(() => {
//     // if (counter > 5) {
//     //   observer.error('too high');
//     // } else {
//       observer.next(++counter);
//   //   }
//   }, 1000);

//   // setTimeout(() => {
//   //   window.clearInterval(h as number);
//   //   observer.complete();
//   // }, 3000);

// });

// o.pipe(
//   map(x => x * 2),
//   filter(x => x > 4),
// ).subscribe(x => console.log(x));


// // const s = o.subscribe(
// //   result => console.log('a', result),
// //   err => { console.log(err); },
// //   () => { console.log('complete'); });

// // console.log(o);
// // console.log(s);
// // s.unsubscribe();

