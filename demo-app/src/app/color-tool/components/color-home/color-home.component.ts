import { Component, DoCheck } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'color-home',
  templateUrl: './color-home.component.html',
  styleUrls: ['./color-home.component.css']
})
export class ColorHomeComponent implements DoCheck {

  headerText = 'Color Tool';
  colors = ['red', 'green', 'blue', 'hot pink'];
  sampleText = new FormControl('');
  sortedColorsCache: string[] = null;

  addColor(newColor: string) {
    console.log(newColor);

    this.colors = this.colors.concat(newColor);
    // this.colors.push(newColor);
  }

  get sortedColors() {

    if (this.sortedColorsCache !== this.colors) {
      console.log('doing sort');
      this.sortedColorsCache = (this.colors = this.colors.map(c => c.toUpperCase()).sort());
    }

    return this.sortedColorsCache;
  }

  ngDoCheck() {
    console.log('change detection is executing');
  }


}
