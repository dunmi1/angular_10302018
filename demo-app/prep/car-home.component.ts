import { Component, OnInit, InjectionToken, Inject } from '@angular/core';

import { Car } from '../../models/car';
import { CarsService } from '../../services/cars.service';

const CarsServiceToken = new InjectionToken('Cars Service');

interface GenericCarService {
  all();
}

class MyCarService extends CarsService {

  private cars = [
    { id: 1, make: 'my test', model: 't', year: 2003, color: 'red', price: 1 }
  ];

  all() {
    console.log('called my all');
    return this.cars;
  }
}

@Component({
  selector: 'car-home',
  templateUrl: './car-home.component.html',
  styleUrls: ['./car-home.component.css'],
  providers: [ { provide: CarsServiceToken, useClass: MyCarService } ],
})
export class CarHomeComponent implements OnInit {

  headerText = 'Car Tool';

  public editCarId = -1;

  constructor(@Inject(CarsServiceToken) private carsSvc: GenericCarService) { }

  ngOnInit() {
  }

  get cars() {
    console.log('test');
    return this.carsSvc.all();
  }

  doAddCar(car: Car) {
    this.editCarId = -1;
    this.carsSvc.append(car);
  }

  doEditCar(carId: number) {
    this.editCarId = carId;
  }

  doDeleteCar(carId: number) {
    this.carsSvc.delete(carId);
    this.editCarId = -1;
  }

  doSaveCar(car: Car) {
    this.carsSvc.replace(car);
    this.editCarId = -1;
  }

  doCancelCar() {
    this.editCarId = -1;
  }

}
