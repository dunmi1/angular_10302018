import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { SharedModule } from '../shared/shared.module';
import { ColorHomeComponent } from './components/color-home/color-home.component';
import { ColorFormComponent } from './components/color-form/color-form.component';
import { MyUppercasePipe } from './pipes/my-uppercase.pipe';
import { MyAppendPipe } from './pipes/my-append.pipe';
import { TypeAheadDemoComponent } from './components/type-ahead-demo/type-ahead-demo.component';

@NgModule({
  imports: [
    CommonModule, ReactiveFormsModule, SharedModule, HttpClientModule,
  ],
  declarations: [ColorHomeComponent, ColorFormComponent, MyUppercasePipe, MyAppendPipe, TypeAheadDemoComponent],
  exports: [ColorHomeComponent, TypeAheadDemoComponent],
})
export class ColorToolModule { }
