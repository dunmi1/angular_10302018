import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { map, tap, switchMap, debounceTime } from 'rxjs/operators';


@Component({
  selector: 'type-ahead-demo',
  templateUrl: './type-ahead-demo.component.html',
  styleUrls: ['./type-ahead-demo.component.css']
})
export class TypeAheadDemoComponent implements OnInit {

  ssnInput = new FormControl('');

  ssns$ = new BehaviorSubject<string>(null);

  persons$: Observable<string[]>;

  // http://localhost:4250/persons?ssn_like=123

  constructor(private httpClient: HttpClient) { }

  ngOnInit() {

    this.persons$ = this.ssns$.pipe(
      debounceTime(500),
      switchMap(ssn => this.httpClient.get<any[]>(`http://localhost:4250/persons?ssn_like=${ssn}`)),
      map(persons => persons.map(p => p.name + '(' + p.ssn + ')'))
    );
  }

  lookupSSN() {
    this.ssns$.next(String(this.ssnInput.value));
  }

}
